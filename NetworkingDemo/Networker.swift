//
//  Networker.swift
//  NetworkingDemo
//
//  Created by Puja Bhagat on 14/12/2021.
//

import Foundation

enum NetworkerError: Error {
    case badResponse
    case badStatusCode(Int)
    case badData
}

class Networker {
    
    static let shared = Networker()
    private let session: URLSession
    
    init() {
        let config = URLSessionConfiguration.default
        session = URLSession(configuration: config)
    }
    
    
    
    func getQuote(completion:@escaping((_ kanya: Kanye?, _ error: Error?) -> Void)){
        guard let url = URL(string: "http://api.kanye.rest") else {
            debugPrint("Invalid Url")
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        
        let task = session.dataTask(with: request){ [unowned self] (data: Data?, response: URLResponse?, error: Error?) in
            
            if let error = error{
                debugPrint(error)
                completion(nil, error)
                return
            }
            
            guard let httpResponse = response as? HTTPURLResponse else {
                debugPrint("not the right response")
                completion(nil, NetworkerError.badResponse)
                return
            }
            
            guard (200...299).contains(httpResponse.statusCode) else {
                debugPrint("Error, status code: \(httpResponse.statusCode)")
                completion(nil, NetworkerError.badStatusCode(httpResponse.statusCode))
                return
            }
            
            guard let data = data else {
                debugPrint("bad data")
                completion(nil, NetworkerError.badData)
                return
            }
            
            do {
                
//                let json = try JSONSerialization.jsonObject(with: data, options: []) as! [String : String]
//                debugPrint(json)
//
                let kanya = try JSONDecoder().decode(Kanye.self, from: data)
                completion(kanya, nil)
            } catch {
                debugPrint(error.localizedDescription)
                completion(nil, error)
            }
            
        }
        task.resume()
    }
    
    
    
    func getImage(completion: @escaping((Data?, Error?) -> Void)){
        let url = URL(string: "https://upload.wikimedia.org/wikipedia/commons/1/10/Kanye_West_at_the_2009_Tribeca_Film_Festival_%28cropped%29.jpg")
        guard let url = url else {
            debugPrint("Invalid Url")
            return
        }
        
        let task = session.downloadTask(with: url) { [unowned self] (localUrl: URL?, response: URLResponse?, error: Error?) in
            
            if let error = error {
                debugPrint("Error: \(error.localizedDescription)")
                completion(nil, error)
                return
            }
            
            guard let httpResponse = response as? HTTPURLResponse else {
                debugPrint("Not the right response")
                completion(nil, NetworkerError.badResponse)
                return
            }
            
            guard (200...299).contains(httpResponse.statusCode) else{
                debugPrint("Error, status code: \(httpResponse.statusCode)")
                completion(nil, NetworkerError.badStatusCode(httpResponse.statusCode))
                return
            }
            
            guard let localUrl = localUrl else {
                completion(nil, NetworkerError.badData)
                return
            }
            do {
                let data = try Data(contentsOf: localUrl)
                completion(data, nil)
            } catch {
                completion(nil, error)
            }
            
        }
        
        task.resume()
    }
}
