//
//  Kanye.swift
//  NetworkingDemo
//
//  Created by Puja Bhagat on 14/12/2021.
//

import Foundation


struct Kanye: Codable {
    let quote: String
}
