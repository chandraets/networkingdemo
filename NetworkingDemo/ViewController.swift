//
//  ViewController.swift
//  NetworkingDemo
//
//  Created by Puja Bhagat on 14/12/2021.
//

/**
 Reference: https://www.youtube.com/watch?v=zvfViYmETuc
 */


import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var lblText: UILabel!
    @IBOutlet weak var btnRandom: UIButton!
    @IBOutlet weak var imgKanye: UIImageView!
    
    //MARK:- PRIVATE METHODS
    
    //MARK:- PUBLIC METHODS
    
    //MARK:- ACTION METHODS
    
    @IBAction func btnRandomAction(_ sender: Any) {
        Networker.shared.getQuote { kenya, error in
            if let error = error {
                debugPrint("Error: \(error.localizedDescription)")
                return
            }
            
            guard let kenya = kenya else {
                return
            }
            
            DispatchQueue.main.async {
                self.lblText.text = kenya.quote
            }
        }

    }
    
    
    //MARK:- OVERRIDE METHODS
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Networker.shared.getImage { (data, error) in
            if let error = error {
                debugPrint("Error: \(error.localizedDescription)")
            }
            
            guard let data = data else {
                return
            }
            
            DispatchQueue.main.async {
                self.imgKanye.image = UIImage(data: data)
            }
        }
    }
    
    //MARK:- DELEGATE METHODS
    
    
    
    
    
}

